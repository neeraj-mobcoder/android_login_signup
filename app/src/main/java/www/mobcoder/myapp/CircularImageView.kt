package www.mobcoder.myapp

import android.os.Bundle
import android.widget.ImageView
import androidx.appcompat.app.AppCompatActivity
import com.bumptech.glide.Glide


class CircularImageView : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_circular_image_view)
        val imageView: ImageView = findViewById<ImageView>(R.id.my_image_view)

        Glide.with(this).load("http://goo.gl/gEgYUd").into(imageView)

    }
}