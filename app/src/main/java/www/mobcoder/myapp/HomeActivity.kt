package www.mobcoder.myapp

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.EditText
import android.widget.TextView
import com.shashank.sony.fancytoastlib.FancyToast
import kotlinx.android.synthetic.main.activity_home.*

class HomeActivity : AppCompatActivity() {
    private lateinit var emailId: TextView
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)
        var b : Bundle? = intent.extras
        var data:String = b?.getString("email").toString()
        emailId = findViewById(R.id.textView5)
        emailId.text = data

        button3.setOnClickListener{
            FancyToast.makeText(this,"Hello World !",FancyToast.LENGTH_LONG, FancyToast.DEFAULT,true).show();
        }
        button5.setOnClickListener{
            FancyToast.makeText(this,"Hello World !",FancyToast.LENGTH_LONG,FancyToast.INFO,true).show();
        }
        button6.setOnClickListener{
            FancyToast.makeText(this,"Hello World !",FancyToast.LENGTH_LONG,FancyToast.SUCCESS,true).show();
        }
        button7.setOnClickListener{
            FancyToast.makeText(this,"Hello World !",FancyToast.LENGTH_LONG,FancyToast.WARNING,true).show();
        }
        button8.setOnClickListener{
            FancyToast.makeText(this,"Hello World !",FancyToast.LENGTH_LONG,FancyToast.ERROR,true).show();
        }
        button9.setOnClickListener{
            FancyToast.makeText(this,"Hello World !",FancyToast.LENGTH_LONG,FancyToast.CONFUSING,true).show();
        }
        button10.setOnClickListener{
            FancyToast.makeText(this,"Hello World !",FancyToast.LENGTH_LONG,FancyToast.SUCCESS,true).show();
        }
        button11.setOnClickListener{
            val i = Intent(this, CircularImageView::class.java)
            startActivity(i)
        }

    }
}