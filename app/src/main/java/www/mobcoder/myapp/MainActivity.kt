package www.mobcoder.myapp

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.EditText
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    lateinit var emailId: EditText
    lateinit var password: EditText
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        emailId = findViewById(R.id.editTextTextEmailAddress)
        password = findViewById(R.id.editTextTextPassword)

        textView3.setOnClickListener {
            val i = Intent(this, SignUpActivity::class.java)
            startActivity(i)
        }

        button.setOnClickListener {
            if (emailId.text.toString().isEmpty() || password.text.toString().isEmpty()) {
                Toast.makeText(applicationContext, "Both Fields are mandatory", Toast.LENGTH_SHORT)
                    .show()
            } else {
                val i = Intent(this, HomeActivity::class.java)
                var b: Bundle = Bundle()
                b.putString("email", "${emailId.text.trim()}")
                i.putExtras(b)
                startActivity(i)
            }

        }


    }
}